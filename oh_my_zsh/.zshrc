# Path to your oh-my-zsh installation.
export ZSH=/Users/zhangbin/.oh-my-zsh

#ZSH_THEME="robbyrussell"
ZSH_THEME="amuse"

plugins=(
  git
#自动补全，安装方法：https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md
  zsh-autosuggestions
#高亮，安装方法：https://github.com/zsh-users/zsh-syntax-highlighting
  zsh-syntax-highlighting
)